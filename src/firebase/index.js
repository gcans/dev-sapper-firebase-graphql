import { firebaseApp$ } from './init'
import {
  signInWithGoogle as _signInWithGoogle,
  signOut as _signOut,
} from './auth'

export const signInWithGoogle = _signInWithGoogle(firebaseApp$)
export const signOut = _signOut(firebaseApp$)

export { firebaseApp$ }