import { writable } from 'svelte/store'
import { request } from '../graphql/request'

export const createQuery = () => {
  const {  set } = writable({})

  return {
   
    useQuery: async query => {
      const { data } = await request(query)
      set(data)
    },
  }
}

export const store = createQuery()